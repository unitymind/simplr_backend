module Api
  module Authentication
    class UnlocksController < ::Devise::UnlocksController
      include Api::ErrorsExtractor

      respond_to :json

      def new
        @error = I18n.t('errors.http.unprocessable_entity')

        render 'api/authentication/error', status: 422
      end

      def create
        self.resource = resource_class.send_unlock_instructions(resource_params)
        yield resource if block_given?

        if successfully_sent?(resource)
          @info = I18n.t('devise.unlocks.send_instructions')

          render 'api/authentication/success', status: 201
        else
          @error = extract_error(resource.errors.messages)

          render 'api/authentication/error', status: 422
        end
      end

      def show
        self.resource = resource_class.unlock_access_by_token(params[:unlock_token])
        yield resource if block_given?

        if resource.errors.empty?
          @info = I18n.t('devise.unlocks.unlocked')

          render 'api/authentication/success', status: 200
        else
          @error = extract_error(resource.errors.messages)

          render 'api/authentication/error', status: 422
        end
      end
    end
  end
end