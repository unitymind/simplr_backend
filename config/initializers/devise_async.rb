Devise::Async.backend = :sidekiq

unless Rails.env.test?
  SimplrData::User.send(:devise, :async)
end