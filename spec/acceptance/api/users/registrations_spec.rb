require 'rspec_api_documentation/dsl'

resource 'User Registrations' do
  header 'Content-Type', 'application/json'

  before(:each) do
    @user = build(:user)
  end

  get '/api/users/sign_up' do
    example 'Disabled GET Sign Up form', document: false do
      do_request

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('errors.http.unprocessable_entity'),
          success: false
        }.to_json
      )
    end
  end

  get '/api/users/edit', document: false do
    include Warden::Test::Helpers

    before(:all) do
      user = create(:user)
      user.confirm!

      login_as user, scope: :api_user
    end

    example 'Disabled GET Edit form' do
      do_request

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('errors.http.unprocessable_entity'),
          success: false
        }.to_json
      )
    end
  end

  get '/api/users/cancel' do
    example_request 'Cancel reigstration process' do
      expect(status).to eq 200


      expect(response_body).to be_json_eql(
        {
          info: I18n.t('messages.registrations.canceled'),
          success: true
        }.to_json
      )
    end
  end

  post '/api/users' do
    parameter :email, 'Email', required: true, scope: :api_user
    parameter :password, 'Password', required: true, scope: :api_user
    parameter :password_confirmation, 'Password confirmation', required: true, scope: :api_user
    parameter :tos_agreement, 'Accepted ToS Agreement', required: true, scope: :api_user

    let(:raw_post) { params.to_json }

    example 'Successful Registration' do

      do_request(api_user: {
        email: @user.email,
        password: @user.password,
        password_confirmation: @user.password_confirmation,
        tos_agreement: @user.tos_agreement
      })

      @user.id = 1

      expect(status).to eq 201

      expect(response_body).to be_json_eql(
        {
          info: I18n.t('devise.registrations.signed_up_but_unconfirmed'),
          success: true,
          user: @user
        }.to_json(except: [:tos_agreement, :invitation_accepted_at, :invitation_created_at, :invitation_limit, :invitation_sent_at, :invitation_token, :invited_by_id, :invited_by_type])
      )
    end

    example 'Already registered email' do

      no_doc do
        do_request(api_user: {
          email: @user.email,
          password: @user.password,
          password_confirmation: @user.password_confirmation,
          tos_agreement: @user.tos_agreement
        })
      end

      do_request(api_user: {
        email: @user.email,
        password: @user.password,
        password_confirmation: @user.password_confirmation,
        tos_agreement: @user.tos_agreement
      })

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('errors.registrations.taken_email'),
          success: false
        }.to_json
      )
    end

    example 'Without Email' do
      do_request(api_user: {
        password: @user.password,
        password_confirmation: @user.password_confirmation,
        tos_agreement: @user.tos_agreement
      })

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('errors.registrations.invalid_email'),
          success: false
        }.to_json
      )
    end

    example 'Invalid Email format' do
      do_request(api_user: {
        email: 'invalid@format',
        password: @user.password,
        password_confirmation: @user.password_confirmation,
        tos_agreement: @user.tos_agreement
      })

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('errors.registrations.invalid_email'),
          success: false
        }.to_json
      )
    end

    example 'Without Password' do
      do_request(api_user: {
        email: @user.email,
        tos_agreement: @user.tos_agreement
      })

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('errors.registrations.password_blank'),
          success: false
        }.to_json
      )
    end

    example 'Password is too short' do
      do_request(api_user: {
        email: @user.email,
        password: Faker::Lorem.characters(7),
        password_confirmation: Faker::Lorem.characters(7),
        tos_agreement: @user.tos_agreement
      })

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('errors.registrations.password_is') + I18n.t('errors.messages.too_short', count: SimplrData::User.password_length.first) + '.',
          success: false
        }.to_json
      )
    end

    example 'Password is too long' do
      password = Faker::Lorem.characters(150)

      do_request(api_user: {
        email: @user.email,
        password: password,
        password_confirmation: password,
        tos_agreement: @user.tos_agreement
      })

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('errors.registrations.password_is') + I18n.t('errors.messages.too_long', count: SimplrData::User.password_length.last) + '.',
          success: false
        }.to_json
      )
    end

    example 'Password confirmation mismatch' do
      do_request(api_user: {
        email: @user.email,
        password: @user.password,
        password_confirmation: 'invalid_password_confirmation',
        tos_agreement: @user.tos_agreement
      })

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('errors.registrations.password_confirmation'),
          success: false
        }.to_json
      )
    end

    example 'Without ToS Agreement' do
      do_request(api_user: {
        email: @user.email,
        password: @user.password,
        password_confirmation: @user.password_confirmation
      })

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('errors.registrations.tos'),
          success: false
        }.to_json
      )
    end
  end

  put '/api/users' do
    include Warden::Test::Helpers

    let(:raw_post) { params.to_json }

    before(:each) do
      @user = create(:user)
      @user.confirm!
      login_as @user, scope: :api_user
    end

    parameter :email, 'Email', required: true, scope: :api_user
    parameter :password, 'Password', required: true, scope: :api_user
    parameter :password_confirmation, 'Password confirmation', required: true, scope: :api_user

    example 'Change email' do
      new_email = Faker::Internet.safe_email

      expect {
        do_request(api_user: {
          email: new_email,
          current_password: @user.password })
      }.to change{ ActionMailer::Base.deliveries.count }.by(1)

      expect(status).to eq 200

      expect(response_body).to be_json_eql(
        {
          info: I18n.t('devise.registrations.update_needs_confirmation'),
          success: true,
          user: @user
        }.to_json(except: [:tos_agreement, :invitation_accepted_at, :invitation_created_at, :invitation_limit, :invitation_sent_at, :invitation_token, :invited_by_id, :invited_by_type])
      )

      # FIXME. Remove to Sessions spec
      no_doc do
        client.post '/api/users/sign_in', api_user: { email: new_email, password: @user.password  }
        expect(status).to eq 422
      end
    end

    example 'Change email with the same', document: false do
      expect {
        do_request(api_user: { email: @user.email, current_password: @user.password })
      }.to_not change{ ActionMailer::Base.deliveries.count }

      expect(status).to eq 200

      expect(response_body).to be_json_eql(
        {
          info: I18n.t('devise.registrations.updated'),
          success: true,
          user: @user
        }.to_json(except: [:tos_agreement, :invitation_accepted_at, :invitation_created_at, :invitation_limit, :invitation_sent_at, :invitation_token, :invited_by_id, :invited_by_type])
      )
    end



    example 'Change email with blank current password' do
      expect {
        do_request(api_user: { email: Faker::Internet.safe_email })
      }.to_not change{ ActionMailer::Base.deliveries.count }

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('errors.registrations.current_password_blank'),
          success: false
        }.to_json
      )
    end

    example 'Change email with invalid current password' do
      expect {
        do_request(api_user: {
          email: Faker::Internet.safe_email,
          current_password: Faker::Lorem.characters(20) })
      }.to_not change{ ActionMailer::Base.deliveries.count }

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('errors.registrations.current_password_invalid'),
          success: false
        }.to_json
      )
    end

    example 'Change password' do
      new_password = Faker::Lorem.characters(20)

      expect {
        do_request(api_user: {
          current_password: @user.password,
          password: new_password,
          password_confirmation: new_password })
      }.to_not change{ ActionMailer::Base.deliveries.count }

      expect(status).to eq 200

      expect(response_body).to be_json_eql(
        {
          info: I18n.t('devise.registrations.updated'),
          success: true,
          user: @user
        }.to_json(except: [:tos_agreement, :invitation_accepted_at, :invitation_created_at, :invitation_limit, :invitation_sent_at, :invitation_token, :invited_by_id, :invited_by_type])
      )
    end


    example 'Change password without current' do
      new_password = Faker::Lorem.characters(20)

      expect {
        do_request(api_user: {
          password: new_password,
          password_confirmation: new_password })
      }.to_not change{ ActionMailer::Base.deliveries.count }

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('errors.registrations.current_password_blank'),
          success: false
        }.to_json
      )
    end

    example 'Change password with invalid current password' do
      new_password = Faker::Lorem.characters(20)

      expect {
        do_request(api_user: {
          current_password: Faker::Lorem.characters(20),
          password: new_password,
          password_confirmation: new_password })
      }.to_not change{ ActionMailer::Base.deliveries.count }

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('errors.registrations.current_password_invalid'),
          success: false
        }.to_json
      )
    end


    example 'Change password with mismatched confirmation' do
      new_password = Faker::Lorem.characters(20)

      expect {
        do_request(api_user: {
          current_password: @user.password,
          password: new_password,
          password_confirmation: Faker::Lorem.characters(20) })
      }.to_not change{ ActionMailer::Base.deliveries.count }

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('errors.registrations.password_confirmation'),
          success: false
        }.to_json
      )
    end

    example_request 'Empty update request' do
      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('errors.registrations.current_password_blank'),
          success: false
        }.to_json
    )
    end
  end

  delete '/api/users' do
    include Warden::Test::Helpers

    before(:all) do
      @user = create(:user)
      @user.confirm!

      login_as @user, scope: :api_user
    end

    example_request 'Delete Account' do
      expect(status).to eq 200

      expect(response_body).to be_json_eql(
        {
          info: I18n.t('devise.registrations.destroyed'),
          success: true
        }.to_json
      )

      no_doc do
        client.post '/api/users/sign_in', api_user: { email: @user.email, password: @user.password  }
        expect(status).to eq 401
      end
    end
  end
end