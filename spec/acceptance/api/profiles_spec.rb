require 'rspec_api_documentation/dsl'

resource 'User Profile' do
  header 'Content-Type', 'application/json'

  before(:each) do
    @user = build(:user)
    @user.skip_confirmation!
    @user.profile = build(:profile)
    @user.save
  end

  get '/api/users/profile' do
    include Warden::Test::Helpers

    example 'Show' do
      login_as @user, scope: :api_user
      do_request

      expect(status).to eq 200

      expect(response_body).to be_json_eql(
        {
          profile: {
            email: @user.email,
            first_name: @user.profile.first_name,
            last_name: @user.profile.last_name,
            appointment: @user.profile.appointment,
            display_as: "#{@user.profile[:first_name]} #{@user.profile[:last_name]}"
          }
        }.to_json
      )
    end
  end

  put '/api/users/profile' do
    include Warden::Test::Helpers

    parameter :first_name, 'First name', scope: :profile
    parameter :last_name, 'Last name', scope: :profile
    parameter :appointment, 'Appointment', scope: :profile

    let(:raw_post) { params.to_json }

    example 'Update' do
      login_as @user, scope: :api_user

      new_profile = FactoryGirl.attributes_for(:profile)
      do_request(profile: new_profile)

      expect(status).to eq 200

      expect(response_body).to be_json_eql(
        {
          profile: {
            email: @user.email,
            first_name: new_profile[:first_name],
            last_name: new_profile[:last_name],
            appointment: new_profile[:appointment],
            display_as: "#{new_profile[:first_name]} #{new_profile[:last_name]}"
          }
        }.to_json
      )
    end
  end
end