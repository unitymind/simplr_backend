require 'devise/failure_app'

module SimplrBackend
  class FailureApp < ::Devise::FailureApp
    def respond
      if warden_options[:recall]
        recall
      else
        self.status = 401
        self.content_type = 'application/json'
        self.response_body = { success: false, error: i18n_message(:invalid) }.to_json
      end
    end
  end
end