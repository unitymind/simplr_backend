require 'sidekiq'
require 'sidekiq-status'

module SimplrBackend
  module Sidekiq
    class << self
      def send_job(owner_id, worker_class, payload = [])
        job = SimplrData::Job.create!(owner_id: owner_id, jid: ::Sidekiq::Client.push('class' => worker_class, 'args' => payload), performed_by: worker_class, args: payload)
        job.update!(status: ::Sidekiq::Status::status(job.jid))
        job
      end

      def get_job(owner_id, jid)
        job = SimplrData::Job.find_by(owner_id: owner_id, jid: jid)
        job.update!(status: ::Sidekiq::Status::status(jid))
        job
      end

      def remove_job(jid)
        job = SimplrData::Job.find_by(jid: jid)
        ::Sidekiq::Status::cancel(job.jid)
        job.destroy
      end
    end

    class DummyJob
      include ::Sidekiq::Worker

      def perform
        Rails.logger.info('FROM DummyJob')
      end
    end
  end
end