require 'rspec_api_documentation/dsl'

resource 'User Sessions' do
  header 'Content-Type', 'application/json'

  before(:each) do
    @user = create(:user)
  end

  get '/api/users/sign_in' do
    example 'Disabled GET Sign In form', document: false do
      do_request

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
             error: I18n.t('errors.http.unprocessable_entity'),
             success: false
         }.to_json
      )
    end
  end

  post '/api/users/sign_in' do
    include Warden::Test::Helpers

    parameter :email, 'Email', required: true, scope: :api_user
    parameter :password, 'Password', required: true, scope: :api_user
    parameter :remember, 'Remember me flag', required: false, scope: :api_user

    let(:raw_post) { params.to_json }

    example 'Successful Logging In' do
      @user.confirm!

      do_request(api_user: { email: @user.email, password: @user.password, remember: false })
      expect(status).to eq 201

      expect(response_body).to be_json_eql(
        {
          info: I18n.t('devise.sessions.signed_in'),
          success: true,
          user: @user
        }.to_json(except: [:tos_agreement, :invitation_accepted_at, :invitation_created_at, :invitation_limit, :invitation_sent_at, :invitation_token, :invited_by_id, :invited_by_type])
      )
    end

    example 'Logging In twice' do
      @user.confirm!
      login_as @user, scope: :api_user

      do_request(api_user: { email: @user.email, password: @user.password, remember: false })

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('devise.failure.already_authenticated'),
          success: false
        }.to_json
      )
    end

    example 'Failed with unconfirmed email' do
      do_request(api_user: { email: @user.email, password: @user.password, remember: false })

      expect(status).to eq 401

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('devise.failure.unconfirmed'),
          success: false
        }.to_json
      )
    end

    example 'Failed with invalid email' do
      do_request(api_user: { email: Faker::Internet.safe_email, password: 'invalid_password', remember: false })

      expect(status).to eq 401

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('devise.failure.not_found_in_database'),
          success: false
        }.to_json
      )
    end

    example 'Failed with invalid password' do
      do_request(api_user: { email: @user.email, password: 'invalid_password', remember: false })

      expect(status).to eq 401

      expect(response_body).to be_json_eql(
        {
           error: I18n.t('devise.failure.invalid'),
           success: false
        }.to_json
      )
    end

    example 'Last attempt warning' do
      no_doc do
        (SimplrData::User.maximum_attempts - 2).times do
          do_request(api_user: { email: @user.email, password: 'invalid_password', remember: false })
          expect(status).to eq 401

          expect(response_body).to be_json_eql(
            {
              error: I18n.t('devise.failure.invalid'),
              success: false
             }.to_json
          )
        end
      end

      do_request(api_user: { email: @user.email, password: 'invalid_password', remember: false })

      expect(status).to eq 401

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('devise.failure.last_attempt'),
          success: false
        }.to_json
      )
    end

    example 'Lock when maximum attempts is reached' do
      no_doc do
        (SimplrData::User.maximum_attempts - 1).times do
          do_request(api_user: { email: @user.email, password: 'invalid_password', remember: false })
          expect(status).to eq 401
        end
      end

      expect {
        do_request(api_user: { email: @user.email, password: 'invalid_password', remember: false })
      }.to change{ ActionMailer::Base.deliveries.count }.by(1)

      expect(status).to eq 401

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('devise.failure.locked'),
          success: false
        }.to_json
      )
    end
  end

  delete '/api/users/sign_out' do
    include Warden::Test::Helpers

    example 'Authenticated user logging out' do

      @user.confirm!
      login_as @user, scope: :api_user

      do_request
      expect(status).to eq 200

      expect(response_body).to be_json_eql(
        {
          info: I18n.t('devise.sessions.signed_out'),
          success: true
        }.to_json
      )
    end

    example 'Not authenticated user logging out' do
      do_request
      expect(status).to eq 401

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('devise.failure.invalid'),
          success: false
        }.to_json
      )
    end
  end

  get '/api/users/current' do
    include Warden::Test::Helpers

    example 'Get current for authenticated user' do
      @user.confirm!
      login_as @user, scope: :api_user

      do_request

      expect(status).to eq 200

      expect(response_body).to be_json_eql(
        {
          info: I18n.t('messages.sessions.current_user'),
          success: true,
          user: @user
        }.to_json(except: [:tos_agreement, :invitation_accepted_at, :invitation_created_at, :invitation_limit, :invitation_sent_at, :invitation_token, :invited_by_id, :invited_by_type])
      )
    end

    example 'Get current for not authenticated user' do
      logout

      do_request

      expect(status).to eq 401

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('devise.failure.invalid'),
          success: false
        }.to_json
      )
    end
  end
end