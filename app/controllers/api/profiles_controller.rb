module Api
  class ProfilesController < ::ApplicationController
    respond_to :json

    before_action :authenticate_api_user!

    def show
      @profile = SimplrBackend::ProfileDecorator.new(current_api_user.profile)
      render 'api/profile', status: 200
    end

    def update
      current_api_user.profile.update!(profile_params)
      @profile = SimplrBackend::ProfileDecorator.new(current_api_user.profile)
      render 'api/profile', status: 200
    end

    private
      def profile_params
        params.require(:profile).permit(:first_name, :last_name, :appointment)
      end
  end
end