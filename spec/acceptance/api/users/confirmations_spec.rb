require 'rspec_api_documentation/dsl'

resource 'User Confirmations' do
  header 'Content-Type', 'application/json'

  before(:each) do
    @user = build(:user)

    no_doc do
      expect {
        client.post '/api/users', api_user: {
          email: @user.email,
          password: @user.password,
          password_confirmation: @user.password_confirmation,
          tos_agreement: @user.tos_agreement
        }
      }.to change {
        ActionMailer::Base.deliveries.count
      }.by(1)
    end

    expect(status).to eq 201

    @user = SimplrData::User.find(JSON.parse(response_body)['user']['id'])
    expect(@user).to_not be_confirmed
  end

  get '/api/users/confirmation/new' do
    example 'Disabled GET request confirmation key form', document: false do
      do_request

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('errors.http.unprocessable_entity'),
          success: false
        }.to_json
      )
    end
  end

  post '/api/users/confirmation' do
    include EmailSpec::Helpers
    include EmailSpec::Matchers

    parameter :email, 'Email', required: true, scope: :api_user

    let(:raw_post) { params.to_json }

    example 'Sending confirmation email' do
      expect {
        do_request(api_user: { email: @user.email })
      }.to change {
        ActionMailer::Base.deliveries.count
      }.by(1)

      expect(status).to eq 201

      expect(response_body).to be_json_eql(
        {
          info: I18n.t('devise.confirmations.send_instructions'),
          success: true
        }.to_json
      )

      mail = ActionMailer::Base.deliveries.last

      expect(mail).to deliver_from Devise.mailer_sender
      expect(mail).to deliver_to @user.email
      expect(mail).to have_subject I18n.t('devise.mailer.confirmation_instructions.subject')

      expect(@user).to_not be_confirmed
    end

    example 'Try to request confirmation for already confirmed account' do
      @user.confirm!

      expect {
        do_request(api_user: { email: @user.email })
      }.to_not change {
        ActionMailer::Base.deliveries.count
      }

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('errors.confirmations.already_confirmed'),
          success: false
         }.to_json
      )
    end

    example 'Invalid email' do
      expect {
        do_request(api_user: { email: Faker::Internet.safe_email })
      }.to_not change {
        ActionMailer::Base.deliveries.count
      }

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('errors.registrations.not_found_email'),
          success: false
        }.to_json
      )

    end
  end

  get '/api/users/confirmation' do
    parameter :confirmation_token, 'Confirmation token from email', required: true

    example_request 'Empty confirmation token' do
      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('errors.confirmations.token_blank'),
          success: false
        }.to_json
      )

      expect(@user.reload).to_not be_confirmed
    end

    example 'Invalid confirmation token' do
      do_request(confirmation_token: 'invalid_confirmation_token')

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('errors.confirmations.token_invalid'),
          success: false
        }.to_json
      )

      expect(@user.reload).to_not be_confirmed
    end

    example 'Use confirmation token from email' do
      expect(@user).to_not be_confirmed

      confirmation_token = ActionMailer::Base.deliveries.last.body.match(/confirmation_token=(.*)"/)[1]

      do_request(confirmation_token: confirmation_token)

      expect(status).to eq 200

      expect(response_body).to be_json_eql(
        {
          info: I18n.t('devise.confirmations.confirmed'),
          success: true
        }.to_json
      )

      expect(@user.reload).to be_confirmed
    end
  end
end