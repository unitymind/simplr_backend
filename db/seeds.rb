# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

consumers = [
  {
    identity: 'google.analytics.reader',
    title: 'Google Analytics Reader',
    oauth2: {
      provider: 'google',
      redirect_uri: "#{ENV['FRONTEND_HOST']}/authentication/oauth2/google",
      scopes: 'https://www.googleapis.com/auth/analytics.readonly email'
    }
  }
]

consumers.each do |settings|
  begin
    consumer = SimplrData::Consumer.create!(identity: settings[:identity], title: settings[:title])
    consumer.settings(:oauth2).update_attributes!(settings[:oauth2])
  rescue ActiveRecord::RecordInvalid
    # ignore
  end
end

users = {}
%w(admin manager agency customer).each do |role|
  user = SimplrData::User.find_or_create_by(email: "#{role}@example.com")
  user.password = ENV['SERVICE_PASSWORD']
  user.password_confirmation =  ENV['SERVICE_PASSWORD']
  user.tos_agreement = true
  user.role = role
  user.skip_confirmation!
  user.save!
  users[role.to_sym] = user
end

users[:manager].update_attributes!(invited_by: users[:admin])
users[:agency].update_attributes!(invited_by: users[:admin])
users[:customer].update_attributes!(invited_by: users[:agency])