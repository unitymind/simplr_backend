# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140923221512) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "settings", force: true do |t|
    t.string   "var",         null: false
    t.text     "value"
    t.integer  "target_id",   null: false
    t.string   "target_type", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "settings", ["target_type", "target_id", "var"], name: "index_settings_on_target_type_and_target_id_and_var", unique: true, using: :btree

  create_table "simplr_data_consumers", force: true do |t|
    t.string   "identity",   null: false
    t.string   "title",      null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "simplr_data_consumers", ["identity"], name: "index_simplr_data_consumers_on_identity", unique: true, using: :btree

  create_table "simplr_data_oauth2_tokens", force: true do |t|
    t.integer  "user_id",          null: false
    t.integer  "consumer_id",      null: false
    t.string   "state"
    t.string   "digest",           null: false
    t.text     "id_token"
    t.string   "provider_user_id"
    t.string   "access_token"
    t.string   "refresh_token"
    t.datetime "expires_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "simplr_data_oauth2_tokens", ["consumer_id"], name: "index_simplr_data_oauth2_tokens_on_consumer_id", using: :btree
  add_index "simplr_data_oauth2_tokens", ["user_id"], name: "index_simplr_data_oauth2_tokens_on_user_id", using: :btree

  create_table "simplr_data_profiles", force: true do |t|
    t.integer  "user_id",     null: false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "appointment"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "simplr_data_profiles", ["user_id"], name: "index_simplr_data_profiles_on_user_id", using: :btree

  create_table "simplr_data_users", force: true do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: ""
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,     null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "tos_agreement",          default: false, null: false
    t.integer  "role"
    t.string   "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invitation_limit"
    t.integer  "invited_by_id"
    t.string   "invited_by_type"
  end

  add_index "simplr_data_users", ["confirmation_token"], name: "index_simplr_data_users_on_confirmation_token", unique: true, using: :btree
  add_index "simplr_data_users", ["email"], name: "index_simplr_data_users_on_email", unique: true, using: :btree
  add_index "simplr_data_users", ["invitation_token"], name: "index_simplr_data_users_on_invitation_token", unique: true, using: :btree
  add_index "simplr_data_users", ["reset_password_token"], name: "index_simplr_data_users_on_reset_password_token", unique: true, using: :btree
  add_index "simplr_data_users", ["unlock_token"], name: "index_simplr_data_users_on_unlock_token", unique: true, using: :btree

end
