module Api
  module ErrorsExtractor
    extend ActiveSupport::Concern
    def extract_error(messages)
      error = ''
      if messages.has_key?(:tos_agreement)
        error = I18n.t('errors.registrations.tos')
      elsif messages.has_key?(:email)
        if messages[:email].include?(I18n.t('errors.registrations.invalid_email'))
          error = I18n.t('errors.registrations.invalid_email')
        elsif messages[:email].include?(I18n.t('errors.messages.taken'))
          error = I18n.t('errors.registrations.taken_email')
        elsif messages[:email].include?(I18n.t('errors.messages.not_found'))
          error = I18n.t('errors.registrations.not_found_email')
        elsif messages[:email].include?(I18n.t('errors.messages.already_confirmed'))
          error = I18n.t('errors.confirmations.already_confirmed')
        elsif messages[:email].include?(I18n.t('errors.messages.not_locked'))
          error = I18n.t('errors.unlocks.not_locked')
        end
      elsif messages.has_key?(:password)
        if messages[:password].include?(I18n.t('errors.messages.blank'))
          error = I18n.t('errors.registrations.password_blank')
        elsif messages[:password].include?(I18n.t('errors.messages.too_short', count: SimplrData::User.password_length.first))
          error = I18n.t('errors.registrations.password_is') + I18n.t('errors.messages.too_short', count: SimplrData::User.password_length.first) + '.'
        elsif messages[:password].include?(I18n.t('errors.messages.too_long', count: SimplrData::User.password_length.last))
          error = I18n.t('errors.registrations.password_is') + I18n.t('errors.messages.too_long', count: SimplrData::User.password_length.last) +'.'
        end
      elsif messages.has_key?(:current_password)
        if messages[:current_password].include?(I18n.t('errors.messages.blank'))
          error = I18n.t('errors.registrations.current_password_blank')
        elsif messages[:current_password].include?(I18n.t('errors.messages.invalid'))
          error = I18n.t('errors.registrations.current_password_invalid')
        end
      elsif messages.has_key?(:password_confirmation)
        error = I18n.t('errors.registrations.password_confirmation')
      elsif messages.has_key?(:reset_password_token)
        if messages[:reset_password_token].include?(I18n.t('errors.messages.blank'))
          error = I18n.t('errors.passwords.token_blank')
        elsif messages[:reset_password_token].include?(I18n.t('errors.messages.invalid'))
          error = I18n.t('errors.passwords.token_invalid')
        end
      elsif messages.has_key?(:unlock_token)
        if messages[:unlock_token].include?(I18n.t('errors.messages.blank'))
          error = I18n.t('errors.unlocks.token_blank')
        elsif messages[:unlock_token].include?(I18n.t('errors.messages.invalid'))
          error = I18n.t('errors.unlocks.token_invalid')
        end
      elsif messages.has_key?(:confirmation_token)
        if messages[:confirmation_token].include?(I18n.t('errors.messages.blank'))
          error = I18n.t('errors.confirmations.token_blank')
        elsif messages[:confirmation_token].include?(I18n.t('errors.messages.invalid'))
          error = I18n.t('errors.confirmations.token_invalid')
        end
      end
      error
    end
  end
end