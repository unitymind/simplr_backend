require 'rspec_api_documentation/dsl'
require 'sidekiq-status/testing/inline'

resource 'Jobs' do
  header 'Content-Type', 'application/json'

  let(:exists_job) { 'SimplrBackend::Sidekiq::DummyJob' }
  let(:not_exists_job) { 'SimplrBackend::Sidekiq::NotExistsJob' }

  before(:each) do
    @user = build(:user)
    @user.skip_confirmation!
    @user.profile = build(:profile)
    @user.save
  end

  post '/api/jobs' do
    include Warden::Test::Helpers

    parameter :worker, 'Worker class', required: true, scope: :job
    parameter :payload, 'Job payload', required: false, scope: :job

    let(:raw_post) { params.to_json }

    example 'Successful schedule new job' do
      login_as @user, scope: :api_user

      expect(SimplrData::Job.count).to eq 0

      do_request(job: {
        worker: exists_job
      })

      expect(status).to eq 200

      expect(SimplrData::Job.count).to eq 1

      job = SimplrData::Job.first

      expect(response_body).to be_json_eql(
        {
          job_id: job.jid,
          status: job.status.to_s
        }.to_json
      )
    end

    example 'Schedule not exists worker' do
      login_as @user, scope: :api_user

      expect(SimplrData::Job.count).to eq 0

      do_request(job: {
        worker: not_exists_job
      })

      expect(status).to eq 422

      expect(SimplrData::Job.count).to eq 0

      expect(response_body).to be_json_eql(
        {
          message: "uninitialized constant #{not_exists_job}"
        }.to_json
      )
    end
  end

  get '/api/jobs/:id' do
    include Warden::Test::Helpers

    example 'Successful get status of exists job' do
      login_as @user, scope: :api_user

      expect(SimplrData::Job.count).to eq 0

      no_doc do
        client.post '/api/jobs', job: {
            worker: exists_job
        }
      end

      expect(SimplrData::Job.count).to eq 1

      job = SimplrData::Job.first

      do_request(id: job.jid)

      expect(status).to eq 200

      expect(response_body).to be_json_eql(
        {
          job_id: job.jid,
          status: job.status.to_s
        }.to_json
      )
    end

    example 'Get status of job with missed id' do
      login_as @user, scope: :api_user

      expect(SimplrData::Job.count).to eq 0

      no_doc do
        client.post '/api/jobs', job: {
            worker: exists_job
        }
      end

      expect(SimplrData::Job.count).to eq 1

      do_request(id: 'missed_id')

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          message: I18n.t('errors.jobs.not_found')
        }.to_json
      )

      expect(SimplrData::Job.count).to eq 1
    end

    example 'Get status of expired job' do
      login_as @user, scope: :api_user

      expect(SimplrData::Job.count).to eq 0

      no_doc do
        client.post '/api/jobs', job: {
            worker: exists_job
        }
      end

      expect(SimplrData::Job.count).to eq 1

      job = SimplrData::Job.first

      allow(::Sidekiq::Status).to receive(:status).and_return(nil)

      do_request(id: job.jid)

      expect(status).to eq 404

      expect(response_body).to be_json_eql(
        {
          message: I18n.t('errors.jobs.not_found')
        }.to_json
      )

      expect(SimplrData::Job.count).to eq 0
    end
  end

  delete '/api/jobs/:id' do
    include Warden::Test::Helpers

    example 'Successful remove job' do
      login_as @user, scope: :api_user

      expect(SimplrData::Job.count).to eq 0

      no_doc do
        client.post '/api/jobs', job: {
          worker: exists_job
        }
        sleep 0.5
      end

      expect(SimplrData::Job.count).to eq 1

      do_request(id: SimplrData::Job.first.jid)

      expect(status).to eq 204

      expect(SimplrData::Job.count).to eq 0
    end

    example 'Remove job with missed id' do
      login_as @user, scope: :api_user

      expect(SimplrData::Job.count).to eq 0

      no_doc do
        client.post '/api/jobs', job: {
            worker: exists_job
        }
        sleep 0.5
      end

      expect(SimplrData::Job.count).to eq 1

      do_request(id: 'missed_id')

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          message: I18n.t('errors.jobs.not_found')
        }.to_json
      )

      expect(SimplrData::Job.count).to eq 1
    end
  end
end