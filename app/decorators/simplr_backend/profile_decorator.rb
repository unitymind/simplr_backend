module SimplrBackend
  class ProfileDecorator < Draper::Decorator
    delegate :first_name, :last_name, :appointment

    def email
      model.user.email
    end

    def display_as
      if !model.first_name.blank? || !model.last_name.blank?
        [model.first_name, model.last_name].join(' ').strip
      else
        self.email
      end
    end
  end
end