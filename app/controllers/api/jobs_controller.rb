module Api
  class JobsController < ::ApplicationController
    respond_to :json

    before_action :authenticate_api_user!

    def create
      begin
        @job = SimplrBackend::Sidekiq::send_job(current_api_user.id, job_params[:worker], job_params[:payload] || [])
        render 'api/job', status: 200
      rescue NameError => ex
        @message = ex.message
        render 'api/error', status: 422
      end
    end

    def destroy
      begin
        job = SimplrBackend::Sidekiq::get_job(current_api_user.id, params[:id])
        SimplrBackend::Sidekiq::remove_job(job.jid)
        render nothing: true, status: 204
      rescue Mongoid::Errors::DocumentNotFound
        @message = I18n.t('errors.jobs.not_found')
        render 'api/error', status: 422
      end
    end

    def show
      begin
        @job = SimplrBackend::Sidekiq::get_job(current_api_user.id, params[:id])

        if @job.status.blank?
          SimplrBackend::Sidekiq::remove_job(@job.jid)
          @message = I18n.t('errors.jobs.not_found')
          render 'api/error', status: 404
        else
          render 'api/job', status: 200
        end
      rescue Mongoid::Errors::DocumentNotFound
        @message = I18n.t('errors.jobs.not_found')
        render 'api/error', status: 422
      end
    end

    private
      def job_params
        params.require(:job).permit(:worker, :payload)
      end
  end
end