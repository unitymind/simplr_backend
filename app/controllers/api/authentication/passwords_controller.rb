module Api
  module Authentication
    class PasswordsController < ::Devise::PasswordsController
      include Api::ErrorsExtractor

      respond_to :json

      def new
        @error = I18n.t('errors.http.unprocessable_entity')

        render 'api/authentication/error', status: 422
      end
      alias :edit :new

      def create
        self.resource = resource_class.send_reset_password_instructions(resource_params)
        yield resource if block_given?

        if successfully_sent?(resource)
          @info = I18n.t('devise.passwords.send_instructions')

          render 'api/authentication/success', status: 201
        else
          @error = extract_error(resource.errors.messages)

          render 'api/authentication/error', status: 422
        end
      end

      def update
        self.resource = resource_class.reset_password_by_token(resource_params)
        yield resource if block_given?

        if resource.errors.empty?
          if resource_params[:password].blank?
            @error = I18n.t('errors.registrations.password_blank')

            render 'api/authentication/error', status: 422
          else
            resource.unlock_access! if unlockable?(resource)
            message_key = resource.active_for_authentication? ? :updated : :updated_not_active

            sign_in(resource_name, resource)

            @info = find_message(message_key)
            @user =  self.resource

            render 'api/authentication/success_with_user', status: 200
          end

        else
          @error = extract_error(resource.errors.messages)

          render 'api/authentication/error', status: 422
        end
      end

      private
        def assert_reset_token_passed
          # bypass parent filter
        end
    end
  end
end