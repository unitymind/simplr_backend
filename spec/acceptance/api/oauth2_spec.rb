require 'rspec_api_documentation/dsl'

resource 'Oauth2 Support' do
  header 'Content-Type', 'application/json'

  let(:google_analytics_reader) { SimplrData::Consumer.find_by!(identity: 'google.analytics.reader') }
  let(:state) { '/path/to_back_redirect' }
  let(:auth_code) { '4/Xc8NCQBf0OVd0tC1BIh_5cpTD0YZ.chWCI5_YUvkcPvB8fYmgkJzBqBJikQI' }
  let(:recurring_auth_code) { '4/usgqr_a6ttjMJqDqClxun8jGh3gU.gskS_QTgidcaPvB8fYmgkJxnyhlikQI' }
  let(:invalid_auth_code) { '4/Xc8NCQBf0OVd0tC1BIh_5cpTD0YZ.chWCI5_YUvkcPvB8fYmgkJzBqBJikQB' }
  let(:raw_post) { params.to_json }
  let(:check_token_fields) { %w(access_token refresh_token id_token provider_user_id) }

  before(:each) do
    @user = build(:user)
    @user.skip_confirmation!
    @user.profile = build(:profile)
    @user.save
  end

  get '/api/oauth2/connect' do
    include Warden::Test::Helpers

    parameter :consume_by, 'Consumer application identity', required: true
    parameter :state, 'Roundtrip state'

    example 'Google' do
      login_as @user, scope: :api_user

      expect(SimplrData::Oauth2Token.count).to eq 0

      do_request(consume_by: google_analytics_reader.identity)

      expect(SimplrData::Oauth2Token.count).to eq 1
      token = SimplrData::Oauth2Token.first

      expect(status).to eq 200
      expect(JSON.parse(response_body)['uri']).to include('https://accounts.google.com/o/oauth2/auth?access_type=offline&approval_prompt=force&client_id=' + ENV['GOOGLE_CLIENT_ID'] + '&redirect_uri=' + token.consumer.settings(:oauth2).redirect_uri + '&response_type=code&scope=https://www.googleapis.com/auth/analytics.readonly%20email&state=' + token.digest)
    end

    example 'Unknown provider' do
      login_as @user, scope: :api_user

      do_request(consume_by: 'unknown')

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          message: I18n.t('errors.oauth2.unknown_consumer', consumer: 'unknown')
        }.to_json
      )
    end

    example 'Not authenticated request' do
      do_request(consume_by: 'unknown')

      expect(status).to eq 401
    end
  end

  post '/api/oauth2/callback' do
    include Warden::Test::Helpers

    parameter :code, 'Authorization code from OAuth2 server', scope: :oauth2
    parameter :state, 'Roundtrip state backed from OAuth2 server', scope: :oauth2
    parameter :error, 'Error field from OAuth2 server', scope: :oauth2

    example 'Invalid state digest' do
      login_as @user, scope: :api_user

      no_doc do
        client.get('/api/oauth2/connect', {consume_by: google_analytics_reader.identity, state: state })
      end

      do_request(oauth2: { code: auth_code, state: 'invalid_digest' } )

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          message: I18n.t('errors.oauth2.invalid_request')
        }.to_json
      )

      expect(SimplrData::Oauth2Token.count).to eql 1
    end

    describe 'Google' do
      example 'First time exchange of authorization code' do
        login_as @user, scope: :api_user

        no_doc do
          client.get('/api/oauth2/connect', {consume_by: google_analytics_reader.identity, state: state })
        end

        token  = SimplrData::Oauth2Token.first

        check_token_fields.each do |field|
          expect(token.send(field.to_sym)).to be_blank
        end

        VCR.use_cassette(:google, record: :new_episodes, match_requests_on: [:method, :uri, :body]) do
          do_request(oauth2: { code: auth_code, state: token.digest } )
        end

        expect(status).to eq 200

        expect(response_body).to be_json_eql(
          {
            message: I18n.t('errors.oauth2.success'),
            state: state
          }.to_json
        )

        token.reload

        check_token_fields.each do |field|
          expect(token.send(field.to_sym)).to_not be_blank
        end
      end

      example 'Recurring exchange of authorization code' do
        login_as @user, scope: :api_user

        no_doc do
          client.get('/api/oauth2/connect', {consume_by: google_analytics_reader.identity, state: state })
        end

        token  = SimplrData::Oauth2Token.first

        no_doc do
          VCR.use_cassette(:google, record: :new_episodes, match_requests_on: [:method, :uri, :body]) do
            client.post '/api/oauth2/callback', oauth2: {
                code: auth_code,
                state: token.digest
            }
          end
        end

        old_data = {
            access_token: token.access_token,
            refresh_token: token.refresh_token,
            expires_at: token.expires_at
        }

        no_doc do
          client.get('/api/oauth2/connect', {consume_by: google_analytics_reader.identity, state: state })
        end

        expect(SimplrData::Oauth2Token.count).to eq 2

        new_token  = SimplrData::Oauth2Token.last

        VCR.use_cassette(:google, record: :new_episodes, match_requests_on: [:method, :uri, :body]) do
          do_request(oauth2: { code: recurring_auth_code, state: new_token.digest } )
        end

        expect(status).to eq 200

        expect(response_body).to be_json_eql(
          {
            message: I18n.t('errors.oauth2.already_connected'),
            state: state
          }.to_json
        )

        expect(SimplrData::Oauth2Token.count).to eq 1

        token.reload

        check_token_fields.each do |field|
          expect(token.send(field.to_sym)).to_not eq(old_data[field.to_sym])
        end
      end

      example 'Access denied by user' do
        login_as @user, scope: :api_user

        no_doc do
          client.get('/api/oauth2/connect', {consume_by: google_analytics_reader.identity, state: state })
        end

        token = SimplrData::Oauth2Token.first

        do_request(oauth2: { state: token.digest, error: 'access_denied' } )

        expect(status).to eq 422

        expect(response_body).to be_json_eql(
          {
            state: state,
            message: I18n.t('errors.oauth2.access_denied')
          }.to_json
        )

        expect(SimplrData::Oauth2Token.count).to eql 0
      end

      example 'Invalid authorization code' do
        login_as @user, scope: :api_user

        no_doc do
          client.get('/api/oauth2/connect', {consume_by: google_analytics_reader.identity, state: state })
        end

        expect(SimplrData::Oauth2Token.count).to eql 1
        token  = SimplrData::Oauth2Token.first

        check_token_fields.each do |field|
          expect(token.send(field.to_sym)).to be_blank
        end

        VCR.use_cassette(:google, record: :new_episodes, match_requests_on: [:method, :uri, :body]) do
          do_request(oauth2: { code: invalid_auth_code, state: token.digest } )
        end

        expect(status).to eq 422

        expect(response_body).to be_json_eql(
          {
            message: I18n.t('errors.oauth2.invalid_request')
          }.to_json
        )

        expect(SimplrData::Oauth2Token.count).to eql 0
      end
    end
  end

  delete '/api/oauth2/revoke/:id' do
    include Warden::Test::Helpers

    example 'Invalid token id' do
      login_as @user, scope: :api_user

      do_request(id: 666)

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          message: I18n.t('errors.oauth2.invalid_request')
        }.to_json
      )
    end

    describe 'Google' do
      example 'Successful revoke' do
        login_as @user, scope: :api_user

        token_id = nil

        VCR.use_cassette(:google, record: :new_episodes, match_requests_on: [:method, :uri, :body]) do
          no_doc do
            client.get '/api/oauth2/connect', {consume_by: google_analytics_reader.identity, state: state }
          end
          token = SimplrData::Oauth2Token.first

          no_doc do
            client.post '/api/oauth2/callback', oauth2: {
                code: auth_code,
                state: token.digest
            }
          end
          token_id = token.id
        end

        expect(SimplrData::Oauth2Token.count).to eq 1

        VCR.use_cassette(:google_nested, record: :new_episodes, match_requests_on: [:method, :uri, :query]) do
          do_request(id: token_id)
        end

        expect(status).to eq 204

        expect(SimplrData::Oauth2Token.count).to eq 0
      end
    end
  end

end