json.connections do
  json.array! @connections do |connection|
    json.identity connection[:consumer].identity
    json.title    connection[:consumer].title

    unless connection[:tokens].empty?
      json.tokens do
        json.array! connection[:tokens] do |token|
          json.id           token.id
          json.email        token.decoded_id_token['email']
          json.created_at token.created_at
          json.updated_at token.updated_at
        end
      end
    end
  end
end
