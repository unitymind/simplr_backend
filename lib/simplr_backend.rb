module SimplrBackend
  autoload :FailureApp, 'simplr_backend/failure_app'
  autoload :Sidekiq, 'simplr_backend/sidekiq'
end