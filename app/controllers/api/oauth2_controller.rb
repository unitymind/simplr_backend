module Api
  class Oauth2Controller < ::ApplicationController
    respond_to :json

    before_action :authenticate_api_user!

    def connect
      authorizer = nil

      begin
        consumer = SimplrData::Consumer.find_by!(identity: uri_params[:consume_by])
        consumer_settings = consumer.settings(:oauth2)

        case consumer_settings.provider
          when 'google'
            authorizer = SimplrServices::Google::Authorizer.build(consumer_settings.redirect_uri)
            authorizer.scope = consumer_settings.scopes

            digest = SimplrData::Oauth2Token.generate_digest(current_api_user.id, consumer.id)

            token = SimplrData::Oauth2Token.create!(digest: digest) do |token|
              token.user_id = current_api_user.id
              token.consumer_id = consumer.id
              token.state = uri_params[:state]
            end

            authorizer.state = token.digest
            @uri = authorizer.authorization_uri({approval_prompt: 'force'}).to_s
            render 'api/oauth2/autorization_uri', status: 200
        end
      rescue ActiveRecord::RecordNotFound
        @message = I18n.t('errors.oauth2.unknown_consumer', consumer: uri_params[:consume_by])
        render 'api/error', status: 422
      end
    end

    def callback
      begin
        token = SimplrData::Oauth2Token.find_by!(user_id: current_api_user.id, digest: callback_params[:state])

        @state = token.state
        consumer_settings = token.consumer.settings(:oauth2)

        case consumer_settings.provider
          when 'google'
            if callback_params[:error].blank?
              authorizer = SimplrServices::Google::Authorizer.build(consumer_settings.redirect_uri)
              authorizer.fetch_by_code!(callback_params[:code], token)
              token.save!
            else
              token.destroy!
            end
        end

        if token.destroyed?
          @message = I18n.t('errors.oauth2.access_denied')
          render 'api/oauth2/callback_error', status: 422
        else
          @message = I18n.t('errors.oauth2.success')
          render 'api/oauth2/callback', status: 200
        end
      rescue ActiveRecord::RecordNotFound
        @message = I18n.t('errors.oauth2.invalid_request')
        render 'api/error', status: 422
      rescue ActiveRecord::RecordInvalid
        old_token = SimplrData::Oauth2Token.find_by!(user_id: token.user_id, consumer_id: token.consumer_id, provider_user_id: token.provider_user_id)
        old_token.access_token = token.access_token
        old_token.refresh_token = token.refresh_token
        old_token.expires_at = token.expires_at
        old_token.save!
        @message = I18n.t('errors.oauth2.already_connected')
        token.destroy
        render 'api/oauth2/callback', status: 200
      rescue Signet::AuthorizationError => e
        Rails.logger.info(e.inspect)
        token.destroy!
        @message = I18n.t('errors.oauth2.invalid_request')
        render 'api/error', status: 422
      end
    end

    def revoke
      begin
        token = current_api_user.oauth2_tokens.find(params[:id])
        authorizer = SimplrServices::Google::Authorizer.build_from_token(token)
        authorizer.revoke!
        token.destroy!
        render nothing: true, status: 204
      rescue ActiveRecord::RecordNotFound, ActiveRecord::RecordNotDestroyed
        @message = I18n.t('errors.oauth2.invalid_request')
        render 'api/error', status: 422
      end
    end

    private
      def uri_params
        params.permit(:consume_by, :state)
      end

      def callback_params
        params.require(:oauth2).permit(:code, :state, :error)
      end
  end
end
