module Api
  class ConnectionsController < ::ApplicationController
    respond_to :json

    before_action :authenticate_api_user!

    def index
      @connections = []
      SimplrData::Consumer.all.each do |consumer|
        connections = { consumer: consumer, tokens: []}
        consumer.oauth2_tokens.where(user_id: current_api_user.id).each do |token|
          connections[:tokens] << token
        end
        @connections << connections
      end

      render 'api/connections/index', status: 200
    end
  end
end