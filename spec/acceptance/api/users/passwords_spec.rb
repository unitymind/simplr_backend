require 'rspec_api_documentation/dsl'

resource 'User Passwords' do
  header 'Content-Type', 'application/json'

  before(:each) do
    @user = create(:user)
    @user.confirm!
  end

  get '/api/users/password/new', document: false do
    example 'Disabled GET Request Password reset form' do
      do_request

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('errors.http.unprocessable_entity'),
          success: false
        }.to_json
      )
    end
  end

  get '/api/users/password/edit', document: false do
    example 'Disabled GET Set new password form' do
      do_request

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('errors.http.unprocessable_entity'),
          success: false
        }.to_json
      )
    end
  end

  post '/api/users/password' do
    include EmailSpec::Helpers
    include EmailSpec::Matchers

    parameter :email, 'Email', required: true, scope: :api_user

    let(:raw_post) { params.to_json }

    example 'Request sending reset email' do
      expect {
        do_request(api_user: { email: @user.email })
      }.to change {
        ActionMailer::Base.deliveries.count
      }.by(1)

      expect(status).to eq 201

      expect(response_body).to be_json_eql(
        {
          info: I18n.t('devise.passwords.send_instructions'),
          success: true
        }.to_json
      )

      mail = ActionMailer::Base.deliveries.last

      expect(mail).to deliver_from Devise.mailer_sender
      expect(mail).to deliver_to @user.email
      expect(mail).to have_subject I18n.t('devise.mailer.reset_password_instructions.subject')
    end

    example 'Invalid email' do
      expect {
        do_request(api_user: { email: Faker::Internet.safe_email })
      }.to_not change {
        ActionMailer::Base.deliveries.count
      }

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('errors.registrations.not_found_email'),
          success: false
        }.to_json
      )
    end
  end

  put '/api/users/password' do
    include Warden::Test::Helpers

    parameter :reset_password_token, 'Reset token from email', required: true, scope: :api_user
    parameter :password, 'New password', required: true, scope: :api_user
    parameter :password_confirmation, 'New password confirmation', required: true, scope: :api_user

    let(:raw_post) { params.to_json }

    before(:each) do
      no_doc do
        expect {
          client.post '/api/users/password', api_user: { email: @user.email }
        }.to change {
          ActionMailer::Base.deliveries.count
        }.by(1)

        expect(status).to eq 201

        @reset_password_token = ActionMailer::Base.deliveries.last.body.match(/reset_password_token=(.*)"/)[1]
        @new_password = Faker::Lorem.characters(20)
      end
    end

    example 'Successful reset' do
      do_request(api_user: {
          reset_password_token: @reset_password_token,
          password: @new_password,
          password_confirmation: @new_password
        }
      )

      expect(status).to eq 200

      expect(response_body).to be_json_eql(
        {
          info: I18n.t('devise.passwords.updated'),
          success: true,
          user: @user
        }.to_json(except: [:tos_agreement, :invitation_accepted_at, :invitation_created_at, :invitation_limit, :invitation_sent_at, :invitation_token, :invited_by_id, :invited_by_type])
      )
    end

    example 'Double usage' do
      no_doc do
        do_request(api_user: {
            reset_password_token: @reset_password_token,
            password: @new_password,
            password_confirmation: @new_password
          }
        )
      end

      logout

      do_request(api_user: {
          reset_password_token: @reset_password_token,
          password: @new_password,
          password_confirmation: @new_password
        }
      )

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('errors.passwords.token_invalid'),
          success: false
        }.to_json
      )
    end

    example 'Empty reset token' do
      do_request(api_user: {
          password: @new_password,
          password_confirmation: @new_password
        }
      )

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('errors.passwords.token_blank'),
          success: false
        }.to_json
      )
    end

    example 'Invalid reset token' do
      do_request(api_user: {
          reset_password_token: 'invalid_reset_password_token',
          password: @new_password,
          password_confirmation: @new_password
        }
      )

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('errors.passwords.token_invalid'),
          success: false
        }.to_json
      )
    end

    example 'Empty password' do
      do_request(api_user: {
          reset_password_token: @reset_password_token
        }
      )

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('errors.registrations.password_blank'),
          success: false
        }.to_json
      )
    end

    example 'Password is too short' do
      new_password = Faker::Lorem.characters(7)
      do_request(api_user: {
          reset_password_token: @reset_password_token,
          password: new_password,
          password_confirmation: new_password
        }
      )

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('errors.registrations.password_is') + I18n.t('errors.messages.too_short', count: SimplrData::User.password_length.first) + '.',
          success: false
        }.to_json
      )
    end

    example 'Password is too long' do
      new_password = Faker::Lorem.characters(150)
      do_request(api_user: {
          reset_password_token: @reset_password_token,
          password: new_password,
          password_confirmation: new_password
        }
      )

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('errors.registrations.password_is') + I18n.t('errors.messages.too_long', count: SimplrData::User.password_length.last) + '.',
          success: false
        }.to_json
      )
    end



  end

end