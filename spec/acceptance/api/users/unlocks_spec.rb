require 'rspec_api_documentation/dsl'

resource 'User Unlocks' do
  header 'Content-Type', 'application/json'

  before(:each) do
    @user = create(:user)

    no_doc do
      (SimplrData::User.maximum_attempts).times do
        client.post '/api/users/sign_in', api_user: {
          email: @user.email,
          password: 'invalid_password'
        }
        expect(status).to eq 401
      end
    end

    @user.reload
  end

  get '/api/users/unlock/new', document: false do
    example 'Disabled GET request unlock key form' do
      do_request

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('errors.http.unprocessable_entity'),
          success: false
        }.to_json
      )
    end
  end

  post '/api/users/unlock' do
    include EmailSpec::Helpers
    include EmailSpec::Matchers

    parameter :email, 'Email', required: true, scope: :api_user

    let(:raw_post) { params.to_json }

    example 'Sending unlock email' do
      expect {
        do_request(api_user: { email: @user.email })
      }.to change {
        ActionMailer::Base.deliveries.count
      }.by(1)

      expect(status).to eq 201

      expect(response_body).to be_json_eql(
        {
          info: I18n.t('devise.unlocks.send_instructions'),
          success: true
        }.to_json
      )

      mail = ActionMailer::Base.deliveries.last

      expect(mail).to deliver_from Devise.mailer_sender
      expect(mail).to deliver_to @user.email
      expect(mail).to have_subject I18n.t('devise.mailer.unlock_instructions.subject')

      expect(@user).to be_access_locked
    end

    example 'Try to request unlock for not locked account' do
      user = create(:user)
      user.confirm!

      expect {
        do_request(api_user: { email: user.email })
      }.to_not change {
        ActionMailer::Base.deliveries.count
      }

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('errors.unlocks.not_locked'),
          success: false
        }.to_json
      )
    end

    example 'Invalid email' do
      expect {
        do_request(api_user: { email: Faker::Internet.safe_email })
      }.to_not change {
        ActionMailer::Base.deliveries.count
      }

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('errors.registrations.not_found_email'),
          success: false
        }.to_json
      )

    end
  end

  get '/api/users/unlock' do
    parameter :unlock_token, 'Unlock token from email', required: true

    example 'Empty unlock token' do
      expect(@user).to be_access_locked

      do_request

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('errors.unlocks.token_blank'),
          success: false
        }.to_json
    )
    end

    example 'Invalid unlock token' do
      expect(@user).to be_access_locked

      do_request(unlock_token: 'invalid_unlock_token')

      expect(status).to eq 422

      expect(response_body).to be_json_eql(
        {
          error: I18n.t('errors.unlocks.token_invalid'),
          success: false
        }.to_json
      )
    end

    example 'Use unlock token from email' do
      expect(@user).to be_access_locked

      unlock_token = ActionMailer::Base.deliveries.last.body.match(/unlock_token=(.*)"/)[1]

      do_request(unlock_token: unlock_token)

      expect(status).to eq 200

      expect(response_body).to be_json_eql(
        {
          info: I18n.t('devise.unlocks.unlocked'),
          success: true
        }.to_json
      )

      expect(@user.reload).to_not be_access_locked
    end
  end
end
