module Api
  module Authentication
    class SessionsController < ::Devise::SessionsController
      include Api::ErrorsExtractor

      respond_to :json

      def new
        if flash.now[:alert]
          @error = flash.now[:alert]

          render 'api/authentication/error', status: 401
        else
          @error = I18n.t('errors.http.unprocessable_entity')

          render 'api/authentication/error', status: 422
        end
      end

      def create
        self.resource = warden.authenticate!(auth_options)
        sign_in(resource_name, resource)
        yield resource if block_given?

        @info = find_message(:signed_in)
        @user = self.resource

        render 'api/authentication/success_with_user', status: 201
      end

      def destroy
        warden.authenticate!(auth_options)

        if Devise.sign_out_all_scopes
          sign_out
        else
          sign_out(resource_name)
        end

        @info = find_message(:signed_out)

        render 'api/authentication/success', status: 200
      end

      def show
        self.resource = warden.authenticate!(auth_options)

        @info = I18n.t('messages.sessions.current_user')
        @user = self.resource

        render 'api/authentication/success_with_user', status: 200
      end

      protected
        # Helper for use in before_filters where no authentication is required.
        #
        # Example:
        #   before_filter :require_no_authentication, only: :new
        def require_no_authentication
          assert_is_devise_resource!
          return unless is_navigational_format?
          no_input = devise_mapping.no_input_strategies

          authenticated = if no_input.present?
                            args = no_input.dup.push scope: resource_name
                            warden.authenticate?(*args)
                          else
                            warden.authenticated?(resource_name)
                          end

          if authenticated && warden.user(resource_name)
            @error = I18n.t('devise.failure.already_authenticated')
            render 'api/authentication/error', status: 422
          end
        end
    end
  end
end