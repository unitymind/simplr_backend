module Api
  module RescueFromInvalidToken
    extend ActiveSupport::Concern

    included do
      rescue_from ActionController::InvalidAuthenticityToken do
        cookies['XSRF-TOKEN'] = form_authenticity_token if protect_against_forgery?
        render json: { error: 'Invalid authenticity token' }, status: :unprocessable_entity
      end
    end
  end
end