require 'sidekiq/web'
require 'sidekiq-status/web'

Rails.application.routes.draw do
  Sidekiq::Web.use Rack::Auth::Basic do |username, password|
    username == 'sidekiq' && password == Rails.application.secrets['sidekiq_password']
  end
  mount Sidekiq::Web => '/sidekiq'

  namespace :api, defaults: {format: :json} do
    devise_for :users, class_name: 'SimplrData::User', module: 'api/authentication', controllers: { registrations: 'api/authentication/registrations' }

    devise_scope :api_user do
      scope :users do
        get 'current', to: 'authentication/sessions#show', as: :user_current
        resource :profile, only: [:show, :update]
      end

      scope :oauth2 do
        get 'connect', to: 'oauth2#connect', as: 'oauth2_connect'
        post 'callback', to: 'oauth2#callback', as: 'oauth2_callback'
        delete 'revoke/:id', to: 'oauth2#revoke', as: 'oauth2_revoke'
        get 'list', to: 'connections#index', as: 'oauth2_connections_list'
      end

      resources :jobs, only: [:show, :create, :destroy]
    end

    # scope module: :v1, constraints: ApiConstraints.new(version: 1, default: :true) do
  end
end
