source 'https://rubygems.org'

ruby '2.0.0', engine: 'jruby', engine_version: '1.7.12'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.1.6'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.3'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0',        group: :doc

gem 'activerecord-jdbcpostgresql-adapter'

# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.7'

gem 'validates_email_format_of', '~> 1.6.0'

gem 'sidekiq', '~> 3.2.2'
gem 'sidekiq-status', '~> 0.5.1'
gem 'sinatra', '~> 1.4.5'
gem 'ledermann-rails-settings', github: 'ledermann/rails-settings', branch: 'master'
gem 'simplr_data', git: 'git@bitbucket.org:unitymind/simplr_data.git', branch: 'staging'
gem 'simplr_services', git: 'git@bitbucket.org:unitymind/simplr_services.git', branch: 'staging'

gem 'devise', '~> 3.2.4'
gem 'devise_invitable', '~> 1.3.6'
gem 'pundit', '~> 0.3.0'
gem 'draper', '~> 1.4.0'
gem 'rack-cors', github: 'cyu/rack-cors', ref: '5fbe64da44db18f1c3a8f28e934e38a9b543698a'
gem 'puma', platforms: [:jruby]
gem 'redis-rails'
gem 'devise-async'
gem 'apitome', github: 'modeset/apitome'
gem 'signet', '~> 0.5.1'
gem 'pry-rails'

group :development, :test do
  gem 'rspec-rails', '~> 3.0.1'
  gem 'factory_girl_rails', '~> 4.4.1'
  gem 'rspec_api_documentation', '~> 4.2.0'
  gem 'better_errors'
  gem 'faker'
  gem 'rack-test'
  gem 'database_cleaner'
  gem 'json_spec'
  gem 'rubocop'
  gem 'brakeman', require: false
end

group :production do
  gem 'rails_12factor'
  gem 'heroku_rails_deflate', '~> 1.0.3'
end

group :test do
  gem 'simplecov', require: false
  gem 'email_spec'
  gem 'webmock', '~> 1.18.0'
  gem 'vcr', '~> 2.9.3'
end
