module Api
  module Authentication
    class RegistrationsController < ::DeviseInvitable::RegistrationsController
      respond_to :json

      include Api::ErrorsExtractor

      def new
        @error = I18n.t('errors.http.unprocessable_entity')

        render 'api/authentication/error', status: 422
      end
      alias :edit :new

      def create
        build_resource(sign_up_params)

        resource_saved = resource.save

        yield resource if block_given?

        if resource_saved
          if resource.active_for_authentication?
            sign_up(resource_name, resource)

            @info = find_message(:signed_up)
            @user = self.resource

            render 'api/authentication/success_with_user', status: 201
          else
            expire_data_after_sign_in!

            @info = find_message("signed_up_but_#{resource.inactive_message}")
            @user = self.resource

            render 'api/authentication/success_with_user', status: 201
          end
        else
          @error = extract_error(resource.errors.messages)

          render 'api/authentication/error', status: 422
        end
      end

      def update
        self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
        prev_unconfirmed_email = resource.unconfirmed_email if resource.respond_to?(:unconfirmed_email)

        resource_updated = update_resource(resource, account_update_params)
        yield resource if block_given?

        if resource_updated
          message_key = update_needs_confirmation?(resource, prev_unconfirmed_email) ? :update_needs_confirmation : :updated
          sign_in resource_name, resource, bypass: true

          @info = find_message(message_key)
          @user = self.resource

          render 'api/authentication/success_with_user', status: 200
        else
          @error = extract_error(resource.errors.messages)

          render 'api/authentication/error', status: 422
        end
      end

      def destroy
        resource.destroy
        Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name)
        yield resource if block_given?

        @info = find_message(:destroyed)

        render 'api/authentication/success', status: 200
      end

      def cancel
        expire_data_after_sign_in!

        @info = I18n.t('messages.registrations.canceled')

        render 'api/authentication/success', status: 200
      end

      private

        def sign_up_params
          params.require(:api_user).permit(:email, :password, :password_confirmation, :tos_agreement)
        end
    end
  end
end